import { PrismaClient } from '@prisma/client';
import seedUsers from './seeds/02_users';
import seedRoles from './seeds/01_roles';

const prisma = new PrismaClient();

async function main() {
  await seedRoles();
  await seedUsers();
}

main()
  .catch((e) => {
    throw e;
  })
  .finally(async () => {
    await prisma.$disconnect();
  });
