import { ApiProperty } from '@nestjs/swagger';
import {User} from '@prisma/client'

const generateNumber = () => {
    return 404
}

const generateLogin = () => {
    return 'vadim'
}

const generatePassword = () => {
    return 'vadimPassword'
}

const generateName= () => {
    return 'Вадим'
}

const generateSecondName = () => {
    return 'Кунгуров'
}

const generateEmail = () => {
    return 'kungurov@gmail.com'
}

export class UserResponse implements User{
    
    @ApiProperty({example:generateNumber()})
    id: number;
    @ApiProperty({example:generateLogin()})
    login: 'vadim';

    @ApiProperty({example:generatePassword()})
    password: string;
    @ApiProperty()
    telephone: string;
    @ApiProperty({example:generateEmail()})
    email: string;
    @ApiProperty({example:generateName()})
    first_name: string;
    @ApiProperty({example:generateSecondName()})
    second_name: string;
    @ApiProperty()
    roleId: string;
    
}