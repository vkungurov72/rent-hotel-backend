import { PrismaClient } from '@prisma/client';
const prisma = new PrismaClient();

export async function seedUsers() {
  const adminRoleId = await prisma.role.findFirst({ where: { name: 'admin' }, select: { id: true } });
  const userRoleId = await prisma.role.findFirst({ where: { name: 'user' }, select: { id: true } });

  await prisma.user.createMany({
    data: [
      {
        login: 'admin',
        password: 'admin',
        email: 'kungurov72@gmail.com',
        first_name: 'Vadim',
        second_name: 'Kungurov',
        roleId: adminRoleId.id,
      },
      {
        login: 'user1',
        password: 'user1',
        email: 'user@gmail.com',
        first_name: 'Lexa',
        second_name: 'West',
        roleId: userRoleId.id,
      },
    ],
  });
}

export default seedUsers