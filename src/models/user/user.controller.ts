import {
  Controller,
  Get,
  Inject,
  Param,
  Query,
  UseGuards,
} from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { UserService } from './user-service';
import { JwtAuthGuard } from '../auth/guard/jwt-auth.guard';
import { User } from '@prisma/client';
import { UserResponse } from 'prisma/swagger/user/user-response';

@ApiTags('User')
@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @ApiOperation({ summary: 'Получить список пользователей' })
  @ApiOkResponse({ type: [UserResponse] })
  // @UseGuards(JwtAuthGuard)
  @Get()
  async getUsers() {
    return this.userService.getUsers();
  }
}
