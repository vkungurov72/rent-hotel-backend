import { HttpException, Injectable } from '@nestjs/common';
import { PrismaService } from 'prisma/postgres/prisma.service';
import { CreateUserDto } from './dto/create-user.dto';
import { HttpStatusCode } from 'axios';
import * as bcrypt from 'bcryptjs';

@Injectable()
export class UserService {
  constructor(private readonly prismaService: PrismaService) {}
  getUsers(): any {
    return this.prismaService.user.findMany();
  }

  getUserByLogin(login: string) {
    return this.prismaService.user.findFirst({
      where: {
        login: login,
      },
    });
  }

  async createUser(newUser: CreateUserDto) {
    if (await this.getUserByLogin(newUser.login)) {
      throw new HttpException(
        'Пользователь с таким логином уже существует',
        HttpStatusCode.BadRequest,
      );
    }

    const hashPassword = await bcrypt.hash(newUser.password, 5);
    return this.prismaService.user.create({
      data: { ...newUser, password: hashPassword },
    });
  }
}
