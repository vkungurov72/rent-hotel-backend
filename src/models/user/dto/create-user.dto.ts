import { User } from '@prisma/client';

export class CreateUserDto implements Omit<User, 'id'> {
  login: string;
  password: string;
  telephone: string;
  email: string;
  first_name: string;
  second_name: string;
  roleId: string;
}

