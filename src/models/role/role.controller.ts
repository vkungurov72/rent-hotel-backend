import { Controller, Get, Req, Res, UseGuards } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { RoleService } from './role.service';

@ApiTags('Role')
@Controller('role')
export class RoleController {
  constructor(private roleService: RoleService) {}

  @Get()
  async getRoles() {
    await this.roleService.getRoles();
  }

  @Get('by-name')
  async getRoleByName(name: string) {
    await this.roleService.getRoleByName(name);
  }
}
