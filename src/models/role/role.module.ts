import { Module } from '@nestjs/common';
import { PrismaModule } from 'prisma/postgres/prisma.module';
import { RoleService } from '../role/role.service';
import { RoleController } from './role.controller';

@Module({
  imports: [PrismaModule],
  controllers: [RoleController],
  providers: [RoleService],
})
export class RoleModule {}
