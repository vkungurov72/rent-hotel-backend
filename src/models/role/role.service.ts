import { Injectable } from '@nestjs/common';
import { PrismaService } from '../../../prisma/postgres/prisma.service';

@Injectable()
export class RoleService {
  constructor(private readonly prismaService: PrismaService) {}

  async getRoles() {
    console.log('roles');
    return this.prismaService.role.findMany();
  }

  async getRoleByName(name: string) {
    return this.prismaService.role.findFirst({
      where: {
        name: name,
      },
    });
  }

  async getRolesById(id: string) {
    return this.prismaService.role.findFirst({
      where: {
        id,
      },
    });
  }
}
