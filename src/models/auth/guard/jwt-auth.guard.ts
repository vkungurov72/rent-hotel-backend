import {
  CanActivate,
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { JwtService } from '@nestjs/jwt';
import JwtConstants from '../constants/jwt.constants';
import { AuthVerifyData } from '../dto/auth-verify.dto';
import { UserData } from '../dto/user-data';

@Injectable()
export class JwtAuthGuard implements CanActivate {
  constructor(private jwtService: JwtService) {}

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const req = context.switchToHttp().getRequest();
    try {
      const cookieToken = req.cookies.userCookieToken;
      if (!cookieToken) {
        throw new UnauthorizedException({
          message: 'Пользователь не авторизован',
        });
      }
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const userDataToken = this.jwtService.verify(cookieToken, {
        secret: JwtConstants.SECRET_KEY,
      }) as AuthVerifyData;

      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const { iat, exp, ...user } = userDataToken;

      req.user = user as UserData;
      return true;
    } catch (e) {
      throw new UnauthorizedException({
        message: 'Пользователь не авторизован',
      });
    }
  }
}
