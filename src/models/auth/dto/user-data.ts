import { User } from '@prisma/client';

export class UserData implements Omit<User, 'password'> {
  id: number;
  login: string;
  telephone: string;
  email: string;
  first_name: string;
  second_name: string;
  roleId: string;
}
