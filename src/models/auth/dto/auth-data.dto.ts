import { User } from '@prisma/client';

export class AuthUserData implements Pick<User, 'login' | 'password'> {
  login: string;
  password: string;
}
