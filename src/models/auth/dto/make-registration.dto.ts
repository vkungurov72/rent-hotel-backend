import { CreateUserDto } from 'src/models/user/dto/create-user.dto';

export class MakeRegistrationUserDto implements Omit<CreateUserDto, 'roleId'> {
  login: string;
  password: string;
  telephone: string;
  email: string;
  first_name: string;
  second_name: string;
  roleId?: string;
  confirmation_password: string;
}
