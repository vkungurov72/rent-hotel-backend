import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { PrismaModule } from 'prisma/postgres/prisma.module';
import { UserService } from '../user/user-service';
import { PassportModule } from '@nestjs/passport';
import { JwtStrategy } from './strategy/auth/jwt.strategy';
import { JwtService } from '@nestjs/jwt';
import { RoleService } from '../role/role.service';

@Module({
  imports: [PrismaModule, PassportModule],
  controllers: [AuthController],
  providers: [
    AuthService,
    UserService,
    JwtStrategy,
    JwtService,
    RoleService,
    RoleService,
  ],
})
export class AuthModule {}
