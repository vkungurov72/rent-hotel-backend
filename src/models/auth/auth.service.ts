import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserService } from '../user/user-service';
import { MakeRegistrationUserDto } from './dto/make-registration.dto';
import { User } from '@prisma/client';
import { AuthUserData } from './dto/auth-data.dto';
import * as bcrypt from 'bcryptjs';
import JwtConstants from './constants/jwt.constants';
import { RoleService } from '../role/role.service';
import { response } from 'express';

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UserService,
    private readonly roleService: RoleService,
  ) {}

  private async generateToken(user: User) {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { password, ...otherInfo } = user;
    return otherInfo;
  }

  async login(userData: AuthUserData) {
    const user = await this.validateUser(userData);
    return await this.generateToken(user);
  }

  async registration(registrationData: MakeRegistrationUserDto) {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { confirmation_password, ...newUser } = registrationData;
    const userRoleId = await this.roleService
      .getRoleByName('user')
      .then((response) => response.id);
    const userData = { ...newUser, roleId: userRoleId };
    const user = await this.userService.createUser(userData);
    return this.generateToken(user);
  }

  private async validateUser(userData: AuthUserData) {
    const user = await this.userService.getUserByLogin(userData.login);
    if (!user) {
      throw new UnauthorizedException({ message: 'Пользователь не найден' });
    }

    const passwordEquals = await bcrypt.compare(
      userData.password,
      user.password,
    );

    if (!passwordEquals) {
      throw new UnauthorizedException({ message: 'Некорректный пароль' });
    }

    return user;
  }
}
