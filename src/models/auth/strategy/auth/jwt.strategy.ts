import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import JwtConstants from '../../constants/jwt.constants';
import { User } from '@prisma/client';
// import { SECRET_KEY_JWT } from '../../constants/secret-key-jwt';
@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor() {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: JwtConstants.SECRET_KEY,
    });
  }

  async validate(user: User) {
    return user;
  }
}
