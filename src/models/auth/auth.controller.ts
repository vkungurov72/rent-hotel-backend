import {
  Body,
  Controller,
  Get,
  Post,
  Res,
  UseGuards,
  Request,
  Req,
} from '@nestjs/common';
import { Request as ExpressRequest } from 'express';
import { CreateUserDto } from '../user/dto/create-user.dto';
import { AuthService } from './auth.service';
import { MakeRegistrationUserDto } from './dto/make-registration.dto';
import { ApiTags } from '@nestjs/swagger';
import JwtConstants from './constants/jwt.constants';
import { JwtService } from '@nestjs/jwt';
import { JwtAuthGuard } from './guard/jwt-auth.guard';
import { RoleService } from '../role/role.service';
import { UserData } from './dto/user-data';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(
    private authService: AuthService,
    private jwtService: JwtService,
    private roleService: RoleService,
  ) {}

  @Post('/login')
  async login(@Body() userDto: CreateUserDto, @Res({ passthrough: true }) res) {
    const payload = await this.authService.login(userDto);
    const token = this.jwtService.sign(payload, {
      secret: JwtConstants.SECRET_KEY,
      expiresIn: 3600000,
    });

    res.cookie('userCookieToken', token, { maxAge: 3600000 });
  }

  @UseGuards(JwtAuthGuard)
  @Get('/validation')
  async validateAuthentication(@Request() request: ExpressRequest) {
    const userData = request.user as UserData;
    const role = await this.roleService.getRolesById(userData.roleId);
    return {
      ...userData,
      role,
    };
  }

  @UseGuards(JwtAuthGuard)
  @Get('/logout')
  async logout(@Req() req, @Res({ passthrough: true }) res) {
    res.clearCookie('userCookieToken');
  }

  @Post('/registration')
  registration(@Body() userDto: MakeRegistrationUserDto) {
    return this.authService.registration(userDto);
  }
}
