import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PrismaModule } from 'prisma/postgres/prisma.module';
import { UserModule } from 'src/models/user/user.module';
import { AuthModule } from './models/auth/auth.module';
import { RoleModule } from './models/role/role.module';

@Module({
  imports: [PrismaModule, UserModule, AuthModule, RoleModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
