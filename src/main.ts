import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { PrismaService } from 'prisma/postgres/prisma.service';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import * as cookieCore from 'cookie-parser';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const prismaService = app.get(PrismaService);

  app.use(cookieCore());

  const config = new DocumentBuilder().build();
  const document = SwaggerModule.createDocument(app, config);
  const documentationPath = 'docs';

  SwaggerModule.setup(documentationPath, app, document);

  await app.listen(4000);
  await prismaService.enableShutdownHooks(app);
}
bootstrap();
